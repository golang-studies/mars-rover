package service

import (
	"encoding/json"
	"io/ioutil"
	"os"
	"reflect"
	"testing"

	"gitlab.com/golang-studies/mars-rover/internal/domain/enums"
	"gitlab.com/golang-studies/mars-rover/internal/domain/models"
)

var (
	Path         string   = "../../../test/resource/service/rover/"
	Instructions []string = []string{"L", "L", "M", "L", "M", "L", "M", "L", "L", "M"}
)

func TestNewRover(t *testing.T) {
	// Test New Rover Service
	jsonFile, err := os.Open(Path + "new_rover.json")
	if err != nil {
		t.Errorf("failed to open json file | %s", err.Error())
	}
	byteValue, _ := ioutil.ReadAll(jsonFile)

	var roverTest *models.Rover
	err = json.Unmarshal([]byte(byteValue), &roverTest)
	if err != nil {
		t.Errorf("failed json file to Rover service New Rover  | %s", err.Error())
	}

	roverService := &RoverService{}
	rover := roverService.NewRover(2, 3, enums.NORTH)
	if err != nil {
		t.Errorf("failed create new rover RoverService | %s", err.Error())
	}

	if !reflect.DeepEqual(roverTest, rover) {
		t.Errorf("Err RoverService create new Rover json invalid")
	}
}

func TestMoveRover(t *testing.T) {
	// Get Rover Berfore Move
	jsonFile, err := os.Open(Path + "move_rover_before.json")
	if err != nil {
		t.Errorf("failed to open json file | %s", err.Error())
	}
	byteValue, _ := ioutil.ReadAll(jsonFile)

	var roverBeforeTest *models.Rover
	err = json.Unmarshal([]byte(byteValue), &roverBeforeTest)
	if err != nil {
		t.Errorf("failed json file to Rover service move rover before | %s", err.Error())
	}

	// Get Rover after Move
	jsonFile, err = os.Open(Path + "move_rover_after.json")
	if err != nil {
		t.Errorf("failed to open json file | %s", err.Error())
	}
	byteValue, _ = ioutil.ReadAll(jsonFile)

	var roverAfterTest *models.Rover
	err = json.Unmarshal([]byte(byteValue), &roverAfterTest)
	if err != nil {
		t.Errorf("failed json file to Rover service move rover after | %s", err.Error())
	}

	// Move rover
	upperDimension := []int{5, 5}
	roverService := &RoverService{}
	roverService.MoveRover(roverBeforeTest, Instructions, upperDimension)

	if !reflect.DeepEqual(roverAfterTest, roverBeforeTest) {
		t.Errorf("Err repository move Rover json invalid")
	}
}

func TestTurnLeft(t *testing.T) {
	cardinalCompassAfter := enums.SOUTH
	cardinalCompassBefore := enums.WEST

	turnLeft(&cardinalCompassBefore)

	if cardinalCompassAfter != cardinalCompassBefore {
		t.Errorf("Err turn left")
	}
}

func TestTurnRigth(t *testing.T) {
	cardinalCompassAfter := enums.WEST
	cardinalCompassBefore := enums.SOUTH

	turnRigth(&cardinalCompassBefore)

	if cardinalCompassAfter != cardinalCompassBefore {
		t.Errorf("Err turn right")
	}
}

func TestMoveForward(t *testing.T) {

	cardinalCompass := enums.WEST
	roverService := &RoverService{}

	rover := roverService.NewRover(2, 3, enums.NORTH)
	localeXAfter := rover.LocaleX - 1

	moveForward(&cardinalCompass, rover)

	if localeXAfter != rover.LocaleX {
		t.Errorf("Err move forward")
	}
}

func TestValidadeCoordinates(t *testing.T) {
	upperDimension := []int{5, 5}
	if !ValidadeCoordinates(3, 2, upperDimension) {
		t.Errorf("Err validate coordinates")
	}
}
