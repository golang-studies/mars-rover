package service

import (
	"errors"

	"gitlab.com/golang-studies/mars-rover/internal/domain/enums"
	"gitlab.com/golang-studies/mars-rover/internal/domain/models"
)

type RoverService struct{}

func (s *RoverService) NewRover(localeX, localeY int, cardinalCompass enums.CardinalCompass) *models.Rover {
	rover := models.NewRover(localeX, localeY, cardinalCompass)
	return rover
}

func (s *RoverService) MoveRover(rover *models.Rover, instructions []string, upperDimension []int) error {

	roverTemp := models.NewRover(rover.LocaleX, rover.LocaleY, rover.CardinalCompass)
	cardinalCompass := rover.CardinalCompass
	for _, inst := range instructions {
		switch inst {
		case string(enums.LEFT):
			turnLeft(&cardinalCompass)
		case string(enums.RIGHT):
			turnRigth(&cardinalCompass)
		case string(enums.MOVE):
			moveForward(&cardinalCompass, roverTemp)
		}
	}
	// Validate New Coordinates before moving rover
	if !ValidadeCoordinates(roverTemp.LocaleX, roverTemp.LocaleY, upperDimension) {
		return errors.New("[Err|Controller-MoveRover] coordinates is not valid!")
	}
	// Update New Coordinates
	rover.LocaleX = roverTemp.LocaleX
	rover.LocaleY = roverTemp.LocaleY
	rover.CardinalCompass = cardinalCompass
	return nil
}

func turnLeft(cardinalCompass *enums.CardinalCompass) {
	switch *cardinalCompass {
	case enums.NORTH:
		*cardinalCompass = enums.WEST
	case enums.SOUTH:
		*cardinalCompass = enums.EAST
	case enums.EAST:
		*cardinalCompass = enums.NORTH
	case enums.WEST:
		*cardinalCompass = enums.SOUTH
	}
}

func turnRigth(cardinalCompass *enums.CardinalCompass) {
	switch *cardinalCompass {
	case enums.NORTH:
		*cardinalCompass = enums.EAST
	case enums.SOUTH:
		*cardinalCompass = enums.WEST
	case enums.EAST:
		*cardinalCompass = enums.SOUTH
	case enums.WEST:
		*cardinalCompass = enums.NORTH
	}
}

func moveForward(cardinalCompass *enums.CardinalCompass, rover *models.Rover) {
	switch *cardinalCompass {
	case enums.NORTH:
		rover.LocaleY += 1
	case enums.SOUTH:
		rover.LocaleY -= 1
	case enums.EAST:
		rover.LocaleX += 1
	case enums.WEST:
		rover.LocaleX -= 1
	}
}

func ValidadeCoordinates(localeX, localeY int, upperDimension []int) bool {
	if localeX > upperDimension[0] || localeY > upperDimension[1] {
		return false
	}

	return true
}
