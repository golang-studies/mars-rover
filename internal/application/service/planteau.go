package service

import (
	"errors"
	"strconv"

	"gitlab.com/golang-studies/mars-rover/internal/domain/enums"
	"gitlab.com/golang-studies/mars-rover/internal/infrastructure/repositories"
)

type PlanteauService struct {
	PlanteauRepository *repositories.PlanteauRepository
	RoverService       *RoverService
}

func NewPlanteauService() *PlanteauService {
	return &PlanteauService{&repositories.PlanteauRepository{}, &RoverService{}}
}

func (s *PlanteauService) Init(upperDimension []int) error {
	planteau, err := repositories.NewPlanteauRepository(upperDimension)
	if err != nil {
		return err
	}
	s.PlanteauRepository = planteau

	return nil
}

func (s *PlanteauService) AddRover(coordinates []string) error {
	localeX, _ := strconv.Atoi(coordinates[0])
	localeY, _ := strconv.Atoi(coordinates[1])

	if !ValidadeCoordinates(localeX, localeY, s.PlanteauRepository.Planteau.UpperDimension) {
		return errors.New("[Err|Service-Planteau] coordinates is not valid!")
	}
	cardinalCompass := enums.StringToCardinalCompass(coordinates[2])

	rover := s.RoverService.NewRover(localeX, localeY, cardinalCompass)
	s.PlanteauRepository.Planteau.Rovers = append(s.PlanteauRepository.Planteau.Rovers, rover)
	return nil
}
