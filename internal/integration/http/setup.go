package http

import (
	"fmt"
	"net/http"

	"gitlab.com/golang-studies/mars-rover/internal/integration/http/handler"
	"gitlab.com/golang-studies/mars-rover/internal/integration/http/routes"
)

func NewServerHttp() *http.ServeMux {
	return http.NewServeMux()
}

func RegisterRoutes(mux *http.ServeMux) {
	h := handler.NewHandler()
	routes.Register(mux, h)
}

func StartServer(mux *http.ServeMux, port string) {
	fmt.Println("........................................................................")
	fmt.Println("... HTTP SERVER MUX ....................................................")
	fmt.Println("... localhost:" + port + " .....................................................")
	fmt.Println("........................................................................")
	http.ListenAndServe(":"+port, mux)
}
