package routes

import (
	"net/http"

	"gitlab.com/golang-studies/mars-rover/internal/integration/http/handler"
)

func Register(mux *http.ServeMux, h *handler.Handler) {
	mux.HandleFunc("/healthCheck", h.HealthCheck)
	mux.HandleFunc("/mars-rover", h.MarsRover)
}
