package enums

type Instruction string

const (
	LEFT  Instruction = "L"
	RIGHT Instruction = "R"
	MOVE  Instruction = "M"
)
