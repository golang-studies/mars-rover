package main

import (
	"os"

	"github.com/joho/godotenv"
	"gitlab.com/golang-studies/mars-rover/internal/integration/http"
)

func main() {
	godotenv.Load()

	httpPort := os.Getenv("HTTP_PORT")
	if httpPort == "" {
		httpPort = "8082"
	}

	server := http.NewServerHttp()
	http.RegisterRoutes(server)
	http.StartServer(server, httpPort)
}
