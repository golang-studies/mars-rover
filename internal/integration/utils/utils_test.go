package utils

import (
	"reflect"
	"testing"
)

func TestSliceAtoi(t *testing.T) {
	slcStr := []string{"1", "2", "3", "4"}
	slcInt := []int{1, 2, 3, 4}

	slcRsp, err := SliceAtoi(slcStr)
	if err != nil {
		t.Errorf("failed parse slice <string> to <int>")
	}

	if !reflect.DeepEqual(slcRsp, slcInt) {
		t.Errorf("Failed parsing slice is not equal to expected")
	}
}
