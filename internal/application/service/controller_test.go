package service

import (
	"reflect"
	"testing"

	"gitlab.com/golang-studies/mars-rover/internal/domain/entities"
)

func TestProgram(t *testing.T) {
	controller := NewControllerService()

	commands := entities.Commands{"5 5", "1 2 N", "LMLMLLM", "3 2 S", "RMRRM", "4 3 S", "LLMLMLLM"}
	roversCoordinates := entities.RoversCoordinate{"0 2 N", "3 2 E", "4 4 E"}
	rovers, err := controller.Program(commands)

	if err != nil {
		t.Errorf("Err init planteau service")
	}

	if !reflect.DeepEqual(roversCoordinates, rovers) {
		t.Errorf("Err Program Invalid coordinates")
	}
}
