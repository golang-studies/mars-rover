package service

import "testing"

func TestInit(t *testing.T) {
	planteauService := NewPlanteauService()

	upperDimension := []int{5, 5}

	err := planteauService.Init(upperDimension)
	if err != nil {
		t.Errorf("Err init planteau service")
	}
}

func TestAddRover(t *testing.T) {
	planteauService := NewPlanteauService()

	upperDimension := []int{5, 5}

	err := planteauService.Init(upperDimension)
	if err != nil {
		t.Errorf("Err init planteau service")
	}

	coordinates := []string{"1", "5", "N"}
	planteauService.AddRover(coordinates)

	if len(planteauService.PlanteauRepository.Planteau.Rovers) < 0 {
		t.Errorf("Err Add rover in planteau service")
	}
}
