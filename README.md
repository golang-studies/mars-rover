# Mars Rover
A squad of robotic rovers are to be landed by NASA on a plateau on Mars. This plateau, which is curiously rectangular, must be navigated by the rovers so that their on-board cameras can get a complete view of the surrounding terrain to send back to Earth.

## Getting started
The application need to have GoLang configured on your machine.
- [Go (lang)](https://go.dev/)

To run application, open `cmd|terminal` in the folder project and run code
```bash
    go run main.go
```

## Documentation
- https://documenter.getpostman.com/view/886263/2s93CExcC7

Example send body: 
```
5 5 // first line upper dimension planteau
1 2 N // coordinates of rover
LMLMLMLMM // instructions to move the above rover 
3 3 E
MMRMMRMRRM
```