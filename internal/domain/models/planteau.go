package models

type Planteau struct {
	UpperDimension []int    // the upper-right coordinates of the plateau
	LowerDimension []int    // the lower-left coordinates
	Rovers         []*Rover // the list of robotic rovers
}

func NewPlanteau(upperDimension, lowerDimension []int, rover []*Rover) *Planteau {
	if rover == nil {
		rover = make([]*Rover, 0)
	}
	return &Planteau{
		upperDimension,
		lowerDimension,
		rover,
	}
}
