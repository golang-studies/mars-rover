package repositories

import (
	"errors"

	"gitlab.com/golang-studies/mars-rover/internal/domain/models"
)

type PlanteauRepository struct {
	Planteau *models.Planteau
}

func NewPlanteauRepository(upperDimension []int) (*PlanteauRepository, error) {
	if len(upperDimension) != 2 {
		return nil, errors.New("[Err|Repositories] Upper Dimension is not valid!")
	}
	rovers := make([]*models.Rover, 0)
	return &PlanteauRepository{Planteau: models.NewPlanteau(upperDimension, []int{0, 0}, rovers)}, nil
}

func (r *PlanteauRepository) AddRover(rover *models.Rover) {
	r.Planteau.Rovers = append(r.Planteau.Rovers, rover)
}
