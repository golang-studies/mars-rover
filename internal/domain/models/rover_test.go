package models

import (
	"encoding/json"
	"io/ioutil"
	"os"
	"reflect"
	"testing"

	"gitlab.com/golang-studies/mars-rover/internal/domain/enums"
)

func TestNewRover(t *testing.T) {
	jsonFile, err := os.Open("../../../test/resource/models/rover.json")
	if err != nil {
		t.Errorf("failed to open json file | %s", err.Error())
	}
	byteValue, _ := ioutil.ReadAll(jsonFile)

	var roverTest *Rover
	err = json.Unmarshal([]byte(byteValue), &roverTest)
	if err != nil {
		t.Errorf("failed json file to models Rover | %s", err.Error())
	}
	localeX := 1
	localeY := 3
	rover := NewRover(localeX, localeY, enums.EAST)

	if !reflect.DeepEqual(roverTest, rover) {
		t.Errorf("Err rover json invalid")
	}
}
