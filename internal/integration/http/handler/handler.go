package handler

import (
	"io"
	"io/ioutil"
	"net/http"
	"strings"

	"gitlab.com/golang-studies/mars-rover/internal/application/service"
)

type Handler struct {
	controller *service.ControllerService
}

func NewHandler() *Handler {
	return &Handler{controller: service.NewControllerService()}
}
func (h *Handler) HealthCheck(w http.ResponseWriter, r *http.Request) {
	io.WriteString(w, "Connect Success")
}

func (h *Handler) MarsRover(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodPost {
		http.Error(w, "[Server] Method Invalid", http.StatusMethodNotAllowed)
		return
	}
	reqBody, err := ioutil.ReadAll(r.Body)
	if err != nil {
		http.Error(w, "[Server] could not read request body", http.StatusBadRequest)
	}
	commands := strings.Split(string(reqBody), "\n")

	rooversCordinate, err := h.controller.Program(commands)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
	}

	io.WriteString(w, strings.Join(rooversCordinate, "\n"))
}
