package enums

type CardinalCompass string

const (
	NORTH CardinalCompass = "N"
	EAST  CardinalCompass = "E"
	SOUTH CardinalCompass = "S"
	WEST  CardinalCompass = "W"
)

func StringToCardinalCompass(s string) CardinalCompass {
	switch s {
	case string(NORTH):
		return NORTH
	case string(EAST):
		return EAST
	case string(SOUTH):
		return SOUTH
	case string(WEST):
		return WEST
	}
	return ""
}
