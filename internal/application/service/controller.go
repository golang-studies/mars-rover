package service

import (
	"fmt"
	"strings"

	"gitlab.com/golang-studies/mars-rover/internal/domain/entities"
	"gitlab.com/golang-studies/mars-rover/internal/integration/utils"
)

type ControllerService struct {
	PlanteauService *PlanteauService
	RoverService    *RoverService
}

func NewControllerService() *ControllerService {
	return &ControllerService{PlanteauService: NewPlanteauService()}
}

func (s *ControllerService) InitPlanteau(upperDimension []int) {
	s.PlanteauService.Init(upperDimension)
}

func (s *ControllerService) DeployRover(coordinates []string) error {
	return s.PlanteauService.AddRover(coordinates)
}

func (s *ControllerService) MoveRover(instructions []string) error {
	idx := len(s.PlanteauService.PlanteauRepository.Planteau.Rovers) - 1

	rover := s.PlanteauService.PlanteauRepository.Planteau.Rovers[idx]
	upperDimension := s.PlanteauService.PlanteauRepository.Planteau.UpperDimension
	s.RoverService.MoveRover(rover, instructions, upperDimension)

	return nil
}

func (s *ControllerService) Program(commands entities.Commands) (entities.RoversCoordinate, error) {
	for idx, command := range commands {
		if idx == 0 {
			upperDimension, _ := utils.SliceAtoi(strings.Split(command, " "))
			s.InitPlanteau(upperDimension)
			continue
		}
		command = strings.TrimSpace(command)
		commandSlice := make([]string, 0)
		if strings.Contains(command, " ") {
			commandSlice = strings.Split(command, " ")
			err := s.DeployRover(commandSlice)
			if err != nil {
				return nil, err
			}
		} else {
			commandSlice = strings.Split(command, "")
			err := s.MoveRover(commandSlice)
			if err != nil {
				return nil, err
			}
		}
	}

	var roversCoordinate entities.RoversCoordinate
	for _, rover := range s.PlanteauService.PlanteauRepository.Planteau.Rovers {
		coordinate := fmt.Sprintf("%d %d %s", rover.LocaleX, rover.LocaleY, rover.CardinalCompass)
		roversCoordinate = append(roversCoordinate, coordinate)
	}

	return roversCoordinate, nil
}
