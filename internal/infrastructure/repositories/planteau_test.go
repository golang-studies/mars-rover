package repositories

import (
	"encoding/json"
	"io/ioutil"
	"os"
	"reflect"
	"testing"

	"gitlab.com/golang-studies/mars-rover/internal/domain/enums"
	"gitlab.com/golang-studies/mars-rover/internal/domain/models"
)

var (
	Path string = "../../../test/resource/repositories/"
)

func TestPlanteauRepository(t *testing.T) {

	// Test Creating the Planteau repository whithout rovers
	jsonFile, err := os.Open(Path + "planteau_repository_without_rovers.json")
	if err != nil {
		t.Errorf("failed to open json file | %s", err.Error())
	}
	byteValue, _ := ioutil.ReadAll(jsonFile)

	var planteauRepositoryWithoutRoversTest *PlanteauRepository
	err = json.Unmarshal([]byte(byteValue), &planteauRepositoryWithoutRoversTest)
	if err != nil {
		t.Errorf("failed json file to repository Planteau Without Rovers | %s", err.Error())
	}
	upperDimension := []int{7, 7}

	planteauRepository, err := NewPlanteauRepository(upperDimension)
	if err != nil {
		t.Errorf("failed create new repository Planteau Without Rovers | %s", err.Error())
	}

	if !reflect.DeepEqual(planteauRepositoryWithoutRoversTest, planteauRepository) {
		t.Errorf("Err repository Planteau Without Rovers json invalid")
	}
}

func TestAddRoverInPlanteauRepository(t *testing.T) {
	// Test Add Rovers in Planteau Repository
	jsonFile, err := os.Open(Path + "planteau_repository_with_rovers.json")
	if err != nil {
		t.Errorf("failed to open json file | %s", err.Error())
	}
	byteValue, _ := ioutil.ReadAll(jsonFile)

	var planteauRepositoryWithRoversTest *PlanteauRepository
	err = json.Unmarshal([]byte(byteValue), &planteauRepositoryWithRoversTest)
	if err != nil {
		t.Errorf("failed json file to repository Planteau With Rovers | %s", err.Error())
	}

	upperDimension := []int{7, 7}

	planteauRepository, err := NewPlanteauRepository(upperDimension)
	if err != nil {
		t.Errorf("failed create new repository Planteau Without Rovers | %s", err.Error())
	}

	rover1 := models.NewRover(1, 3, enums.NORTH)
	rover2 := models.NewRover(5, 1, enums.EAST)
	planteauRepository.AddRover(rover1)
	planteauRepository.AddRover(rover2)

	if !reflect.DeepEqual(planteauRepositoryWithRoversTest, planteauRepository) {
		t.Errorf("Err repository Planteau With Rovers json invalid")
	}
}
