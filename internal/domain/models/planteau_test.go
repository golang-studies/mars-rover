package models

import (
	"encoding/json"
	"io/ioutil"
	"os"
	"reflect"
	"testing"
)

func TestNewPlanteau(t *testing.T) {

	jsonFile, err := os.Open("../../../test/resource/models/planteau.json")
	if err != nil {
		t.Errorf("failed to open json file | %s", err.Error())
	}
	byteValue, _ := ioutil.ReadAll(jsonFile)

	var planteauTest *Planteau
	err = json.Unmarshal([]byte(byteValue), &planteauTest)
	if err != nil {
		t.Errorf("failed json file to models Planteau | %s", err.Error())
	}
	upperDimension := []int{5, 5}
	lowerDimension := []int{0, 0}
	planteau := NewPlanteau(upperDimension, lowerDimension, nil)

	if !reflect.DeepEqual(planteauTest, planteau) {
		t.Errorf("Err planteau json invalid")
	}
}
