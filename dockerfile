FROM golang:1.20 as build

WORKDIR /app

COPY . .

RUN go mod download

RUN go build -o mars-rover ./main.go

FROM alpine:latest

COPY --from=build /app/mars-rover  /app/mars-rover 
COPY --from=build /app/.env /app/.env

RUN chmod +x app/mars-rover 

CMD ["./app/mars-rover"]