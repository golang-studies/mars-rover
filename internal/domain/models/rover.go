package models

import (
	"gitlab.com/golang-studies/mars-rover/internal/domain/enums"
)

type Rover struct {
	LocaleX         int
	LocaleY         int
	CardinalCompass enums.CardinalCompass
}

func NewRover(localeX, localeY int, cardinalCompass enums.CardinalCompass) *Rover {
	return &Rover{LocaleX: localeX, LocaleY: localeY, CardinalCompass: cardinalCompass}
}
